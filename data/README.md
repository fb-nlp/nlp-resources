# Fontes dos dados

- [Natura do Minho](http://natura.di.uminho.pt/download/sources/Dictionaries/wordlists/)   
- [VERO - LibreOffice grammar checker](https://pt-br.libreoffice.org/projetos/vero/)      
- [FalaBrasil language model](https://gitlab.com/fb-asr/fb-asr-resources)                 
- [FalaBrasil audio corpora](https://gitlab.com/fb-audio-corpora)                         
- [FrequencyWords repo on GitHub](https://github.com/hermitdave/FrequencyWords) (baseada em https://www.opensubtitles.org)

## Disclaimer
- Palavras com caracteres incorretos (`àèî`) como *paisà*, *valisère*, e
  *boîtes* foram mantidas. Ao realizar o comando abaixo, é possível ver os
caracteres que não pertencem ao Português Brasileiro:

```bash
$ cat wordlist-huge.txt | tr -d '[a-z\-]' | sed -e 's/[áãâõóôêéíúç]//g' | sort | uniq
``` 

- Algumas palavras foram consertadas manualmente na lista de palavras do 
Projeto Natura da Universidade do Minho. O principal padrão de erros 
manualmente corrigidos é mostrado na Tabela abaixo:

| antes (PT\_EU)| depois (PT\_BR)  |
|:-------------:|:----------------:|
| confecionar   | confe**c**cionar |
| antepo-la     | antep**ô**-la    |
| compo-la      | comp**ô**-la     |
| antissética   | antissé**p**tica |
| concetual     | conce**p**tual   |
| construira    | constru**í**ra   |
| dececionar    | dece**p**cionar  |
| receção       | rece**ss**ão     |

- Ao executar o script `scripts/run_them_all.sh`, o arquivo `wordlist.txt`
  criado é o filtrado pelo Aspell. O arquivo passado como parâmetro contém
  apenas pequenos filtros de expressões regulares, portanto contém várias
  palavras que definitivamente nem existem. Para mais informações veja o
  diretório [`scripts/`](./scripts). Para informações mais precisas, não há
  nada melhor do que ler código :blush:


[![FalaBrasil](../doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](../doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2020)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Cassio Batista - https://cassota.gitlab.io/    
