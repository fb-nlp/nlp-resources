#!/bin/bash
#
# Grupo FalaBrasil (2020)
# Federal Univerisity of Pará (UFPA)
#
# author: may 2020
# cassio batista - https://cassota.gitlab.io/

DEGUB=true  # to speed things up a bit

echo -e "\033[94m  ____                         \033[93m _____     _           \033[0m"
echo -e "\033[94m / ___| _ __ _   _ _ __   ___  \033[93m|  ___|_ _| | __ _     \033[0m"
echo -e "\033[94m| |  _ | '__| | | | '_ \ / _ \ \033[93m| |_ / _\` | |/ _\` |  \033[0m"
echo -e "\033[94m| |_| \| |  | |_| | |_) | (_) |\033[93m|  _| (_| | | (_| |    \033[0m"
echo -e "\033[94m \____||_|   \__,_| .__/ \___/ \033[93m|_|  \__,_|_|\__,_|    \033[0m"
echo -e "                  \033[94m|_|      \033[32m ____                _ _\033[0m\033[91m  _   _ _____ ____    _   \033[0m"
echo -e "                           \033[32m| __ ) _ __ __ _ ___(_) |\033[0m\033[91m| | | |  ___|  _ \  / \          \033[0m"
echo -e "                           \033[32m|  _ \| '_ / _\` / __| | |\033[0m\033[91m| | | | |_  | |_) |/ ∆ \        \033[0m"
echo -e "                           \033[32m| |_) | | | (_| \__ \ | |\033[0m\033[91m| |_| |  _| |  __// ___ \        \033[0m"
echo -e "                           \033[32m|____/|_|  \__,_|___/_|_|\033[0m\033[91m \___/|_|   |_|  /_/   \_\       \033[0m"
echo -e "                                     https://ufpafalabrasil.gitlab.io/"

if test $# -ne 1 ; then
    echo "usage: $0 <out-file>"
    echo "  <out-file> is a text file where all individual word lists will"
    echo "             be merged into"
    echo "  e.g.: ./$0 wordlist-huge.txt"
    exit 1
fi

# my workspace dir
ws_dir=res_temp

mkdir -p $ws_dir || exit 1

echo "[$0] natura do minho" | lolcat
./00_acquisition/get_natura.sh $ws_dir/natura.txt
if $DEGUB ; then
    echo "[$0] WARNING: debug mode on. a dummy file will be processed." | lolcat
    shuf -n 10000 $ws_dir/natura.txt | sort > /tmp/natura.dummy
    ./01_src2wlist/natura2wlist.sh /tmp/natura.dummy $ws_dir/natura_processed.txt
else
    ./01_src2wlist/natura2wlist.sh $ws_dir/natura.txt $ws_dir/natura_processed.txt
fi

echo "[$0] LibreOffice VERO" | lolcat
./00_acquisition/get_vero.sh $ws_dir/vero.txt
if $DEGUB ; then
    echo "[$0] WARNING: debug mode on. a dummy file will be processed." | lolcat
    shuf -n 10000 $ws_dir/vero.txt | sort > /tmp/vero.dummy
    ./01_src2wlist/vero2wlist.sh /tmp/vero.dummy $ws_dir/vero_processed.txt
else
    ./01_src2wlist/vero2wlist.sh $ws_dir/vero.txt $ws_dir/vero_processed.txt
fi

echo "$0: FalaBrasil LM" | lolcat
./00_acquisition/get_lm.sh $ws_dir/lm.arpa
./01_src2wlist/lmarpa2wlist.py $ws_dir/lm.arpa $ws_dir/fblm_processed.txt

corpora=( "alcaim16k-DVD1de4" "alcaim16k-DVD2de4" "alcaim16k-DVD3de4"
          "alcaim16k-DVD4de4" "spoltech16k"       "westpoint16k"
          "lapsbm16k" "codigodefesaconsumidor16k" "constituicao16k" "lapsstory16k" )
for dir in ${corpora[@]} ; do
    echo "[$0] FalaBrasil audio corpora: $dir" | lolcat
    outfile=$(basename $dir)
    ./01_src2wlist/corptrans2wlist.py \
        ${HOME}/fb-gitlab/fb-audio-corpora/$dir ${outfile}_processed.txt
done

rm -f $ws_dir/fbcorpora_processed.txt
for dir in ${corpora[@]} ; do
    outfile=$(basename $dir)
    cat ${outfile}_processed.txt >> $ws_dir/fbcorpora_processed.txt
    rm -f ${outfile}_processed.txt
done

echo "$0: Most frequent words"
./00_acquisition/get_mostfreq.sh $ws_dir/mostfreq_processed.txt

sort $ws_dir/*_processed.txt | uniq > $1

echo "$0: filtering" | lolcat
./02_filtering/filter_misspell.sh $1

echo "$0: filtering mispelled words" | lolcat
./02_filtering/rm_mispell.py list.txt mispellings.txt
rm list.txt mispellings.txt

echo "$0: done " | lolcat
rm -f .keep_running
exit 0
