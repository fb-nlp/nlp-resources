# Scripts de processamento de lista de palavras (texto)

Execute o script `run_them_all.sh` com a variável DEBUG setada como `true` para
ter um _feeling_  do recurso que será gerado. Depois sete a variável como
`false` para gerar a maior lista de palavras que conseguimos até agora. Cuidado
que ele vai demorar algumas horas usando as 6 _threads_ setadas de forma padrão
nos scripts.

## 
- [`00_acquisition/`](./00_acquisition): scripts that download (get) wordlists
  from some remote server. Those scripts will likely generate a single list of
  raw, unprocessed words, which we'll call "source" list.
- [`01_src2wlist/`](./01_filtering): scripts that either process "source" lists
  of words downloaded from a remote server via scripts from `00_*`, or scan a 
  local directory in your machine, looking for `.txt` files in order to extract
  words from them. Each script will individually generate its own wordlist,
  which in the end shall be concatenated, sorted a 'uniq'ed" to a single one
  that combines them all.
- [`02_filtering/`](./02_filtering): scripts that use 
  [Aspell](http://aspell.net/) ([Hunspell](http://hunspell.github.io/) is
  commented in the code because it used VERO's dictionary, whose list of words
  is being used, but can be used as well) to filter misspelled words from the
  huge wordlist created by scripts from `01_*`.

[![FalaBrasil](../../doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](../../doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2020)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Cassio Batista - https://cassota.gitlab.io/    
