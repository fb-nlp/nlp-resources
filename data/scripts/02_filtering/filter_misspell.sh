#!/bin/bash
#
# Grupo FalaBrasil (2020)
# Universidade Federal do Pará (UFPA)
#
# This scripts runs in conjunction with 'rm_mispell.py'.
# This script receives a list of words with its respective frequency count,
# and filters it using grep regex, aspell and hunspell to generate a list of
# mispeelings. That list is then fed to a filter script in python to remove
# mispelling from the original frequency list, generating a last the file
# 'list.txt'. By the time of the writing of this script the most up-to-date
# and easy-to-find frequency list in PT_BR was extracted from
# https://github.com/hermitdave/FrequencyWords/blob/master/content/2018/pt_br/pt_br_full.txt
#
# author: may 2020
# cassio batista - https://cassota.gitlab.io/

if test $# -ne 1 ; then
    echo "usage: $0 <word-list>"
    exit 1
fi

# check if aspell is installed and supports brazilian portuguese
if ! type -t aspell > /dev/null ; then
    echo "$0: please install aspell with support to PT_BR"
    exit 1
elif [ -z "$(aspell dicts | grep pt_BR)" ] ; then
    echo "$0: please install aspell with support to PT_BR"
    exit 1
fi

## check if hunspell is installed and supports brazilian portuguese
#if ! type -t hunspell > /dev/null ; then
#    echo "$0: please isntall hunspell with support to PT_BR"
#    exit 1
#elif [ -z "$(hunspell -D 2>&1 | grep 'AVAILABLE DICTIONARIES' -A30 | grep pt_BR)" ] ; then
#    echo "$0: please isntall hunspell with support to PT_BR"
#    exit 1
#fi

# https://stackoverflow.com/questions/11577720/remove-lines-that-contain-non-english-ascii-characters-from-a-file
echo "$0: sorting and grepping out non pt_br chars"
export LC_ALL=C
sort $1 | awk '{print $1}' | sed 's/ü/u/g' | grep '^[a-zàáéíóúâêôãõç]*$' > out
mv out list.txt

# https://unix.stackexchange.com/questions/70933/regular-expression-for-finding-double-characters-in-bash
# https://stackoverflow.com/questions/3785375/how-to-convert-text-file-to-lowercase-in-unix-but-in-utf-8
echo "$0: grepping out triple chars and forcing lower case"
export LC_ALL=pt_BR.UTF-8
egrep -v '(.)\1{2}' list.txt | sed -e 's/./\L\0/g' > out
mv out list.txt

echo "$0: generating list of mispelled words with aspell"
cat list.txt | \
    aspell -l pt_br list > mispellings.txt
    #hunspell -L -d pt_BR | \

echo "$0: done!"
exit 0
