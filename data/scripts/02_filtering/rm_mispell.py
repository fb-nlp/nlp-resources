#!/usr/bin/env python3
# 
# Grupo FalaBrasil (2020)
# Universidade Federal do Pará (UFPA)
#
# This scripts depends on 'aspelling.sh'.
# This script receives a list of words, supposedly filtered by 'aspelling.sh'
# and a list of mispelled words, produced by aspell (see aspelling.sh). The
# script then cleans out the word list getting rid of the mispellings.
#
# author: may 2020
# cassio batista - https://cassota.gitlab.io/

import sys
import os

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: %s <word-list> <mispelling-list>')
        print('  <word-list> is a text file with a list of ' + \
              'words, one per line')
        print('  <mispelling-list> is a text file with a list of ' + \
              'mispelled words, one per line')
        sys.exit(1)

print('%s: loading word list' % sys.argv[0])
with open(sys.argv[1], 'r') as l:
    wordlist = l.read().split()

print('%s: loading list of mispellings' % sys.argv[0])
with open(sys.argv[2], 'r') as m:
    mispellings = m.read().split()

m = mispellings.pop(0)
with open('wordlist.txt', 'w') as f:
    for i, w in enumerate(wordlist):
        if w == m:
            if len(mispellings):
                m = mispellings.pop(0)
            else:
                m = None
            continue
        f.write('%s\n' % w)
        sys.stdout.write('\r%s: removing mispellings %d / %d ' %
                         (sys.argv[0], i, len(wordlist)))
        sys.stdout.flush()
print()
