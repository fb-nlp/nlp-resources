#!/bin/bash
#
# a script to download all sources for worlist to go through post processing
# scripts in order to generate a huge list of words
#
# author: may 2020
# cassio batista - https://cassota.gitlab.io/

URL=https://gitlab.com/fb-nlp/nlp-resources/-/raw/master/res/lm.3gram.arpa.gz

if test $# -ne 1 ; then
    echo "usage: $0 <out-file>"
    echo "  <out-file> is the txt output file that will store the natura do "
    echo "             minho's big wordlist"
    exit 1
fi

OUTFILE=$1

filename=$(basename $URL)
if [[ ! -f "$filename" ]] ; then
    echo "[$0] downloading '$filename' from server..."
    wget -q --show-progress $URL
else
    echo "[$0] file '$filename' exists and will NOT be downloaded"
fi

echo "[$0] uncompressing '$filename' to '$OUTFILE'..."
gzip -kdc $filename > $OUTFILE

echo "[$0] done!"
exit 0
