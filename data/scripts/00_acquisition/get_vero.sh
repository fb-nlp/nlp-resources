#!/bin/bash
#
# a script to download all sources for worlist to go through post processing
# scripts in order to generate a huge list of words
#
# author: oct 2019
# cassio batista - https://cassota.gitlab.io/

URL=https://pt-br.libreoffice.org/assets/Uploads/PT-BR-Documents/VERO/VeroptBRV320AOC.oxt

if test $# -ne 1 ; then
    echo "usage: $0 <out-file>"
    echo "  <out-file> is the txt output file that will store VERO's "
    echo "             big wordlist"
    exit 1
fi

OUTFILE=$1

filename=$(basename $URL)
if [[ ! -f "$filename" ]] ; then
    echo "[$0] downloading '$filename' from server..."
    wget -q --show-progress $URL
else
    echo "[$0] file '$filename' exists and will NOT be downloaded"
fi

tmpdir=$(mktemp -d)
echo "[$0] uncompressing '$filename' at '${tmpdir}/', creating '$OUTFILE'..."
unzip -q $filename -d $tmpdir
cp ${tmpdir}/pt_BR.dic $OUTFILE

notify-send "'$0' finished" || echo "[$0] done!"
exit 0
