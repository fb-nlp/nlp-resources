#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# author: feb 2019
# cassio batista - cassio.batista.13@gmail.com

import sys
import re

if len(sys.argv) != 3:
	print('A script to extract a word list from the LibreOffice\'s VERO package ' + \
				'(VERificador Ortográfico or Grammar Checker)')
	print('usage: $ python3 %s <vero_dic> <wlist_out>' % sys.argv[0])
	print('\t<vero_dic>:  input file (typically \'pt_BR.dic\')')
	print('\t<wlist_out>: output file')
	sys.exit(1)

# TODO handle states abbrev such as SP MA PA RJ RS MT MG AM
bad = [ 'µ', 'å', 'è', 'ë', 'î', 'ï', 'ñ', 'º', 'ú', '.', 'ö', '²', '³' ]
print('loading input file %s ...' % sys.argv[1])
with open(sys.argv[1], 'r', encoding='utf-8') as f:
	tmp = f.readlines()
	in_list = []
	for line in tmp:
		for word in line.split():
			in_list.append(word)
	del tmp

out_list = []
for word in in_list:
	word = re.sub('[0-9]', '', word)
	word = word.rstrip().lower().replace('\'','')
	flag = False
	for c in bad:
		if c in word:
			flag = True
			break
	if not flag and len(word) > 1 and word.count('-') < 3 :
		if '/' in word:
			word = word.split('/')[0]
		out_list.append(word)

print('writing output file %s ...' % sys.argv[2])
with open(sys.argv[2], 'w') as f:
	for word in out_list:
		f.write(word.replace('ü','u') + '\n')
