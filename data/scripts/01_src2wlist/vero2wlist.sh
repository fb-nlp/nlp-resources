#!/bin/bash
#
# A dumb script to convert the libreoffice official dictionary for Brazilian
# Portuguese (actually, the VERO package, which stands for VERificador
# Ortográfico) to a list of words.
#
# Author: Mar 2018
# Cassio Batista - cassio.batista.13@gmail.com
# Federal University of Pará (UFPA). Belém, Brazil.
#
# "References"
# [1] https://pt-br.libreoffice.org/projetos/vero/
# [2] https://stackoverflow.com/questions/18602234/
#     sed-to-remove-everything-after-in-file-using-command
# [3] https://stackoverflow.com/questions/10358547/
#     grep-for-contents-after-pattern/10358949#10358949
# [4] https://stackoverflow.com/questions/18041761/
#     bash-need-to-test-for-alphanumeric-string
# [5] https://stackoverflow.com/questions/229551/string-contains-in-bash
# [6] https://stackoverflow.com/questions/18488270/
#     how-to-check-the-first-character-in-a-string-in-unix
# [7] https://stackoverflow.com/questions/3785375/
#     how-to-convert-text-file-to-lowercase-in-unix-but-in-utf-8
# [8] https://rimuhosting.com/knowledgebase/linux/misc/trapping-ctrl-c-in-bash
# [9] # https://www.portugal-a-programar.pt/forums/topic/
#       19646-lista-de-palavras-dicion%C3%A1rio-de-palavras-em-
#       portugu%C3%AAs-de-portugal/?do=findComment&comment=311910

NJ=6
PREFIX='VERO'

if test $# -ne 2 ; then
    echo "$0 is a dumb script to convert the libreoffice (lo) official"
    echo "dictionaty for Brazilian Portuguese (actually, the VERO package,"
    echo "which stands for 'VERificador Ortográfico') to a list of words."
    echo
    echo "usage: $0 <LO_pt-br.dic> <wlist.txt>"
    echo -e "\t<LO_pt-br.dic> is the LibreOffice dict for PT_BR (VERO pkg)" # [1]
    echo -e "\t<wlist.txt>    is a word list extracted from the dictionary"
    exit 1
fi

trap ctrl_c INT # [8]
touch .keep_running
function ctrl_c() {
    echo ""
    echo "** Trapped CTRL-C" > /dev/null
    rm -f .keep_running
    sleep 0.2
    rm -f *.temp *.text
    exit 0
}

function pre_process() {
    encoding=$(file -i $1 | grep -oP '(?<=charset=).*$') # [3]
    tmpfile=$(mktemp)
    if [[ "$encoding" == "utf-8" ]] ; then
        cp $1 > $tmpfile
    elif [[ "$encoding" == "unknown-8bit" ]] ; then
        # https://unix.stackexchange.com/questions/184538/how-to-convert-unknown-8bit-file-to-utf8
        # https://www.linuxquestions.org/questions/linux-software-2/how-to-make-iconv-to-skip-incorrect-symbols-or-iconv-alternative-893435/
        #iconv -f cp1256 -t utf-8 $1 | grep -vE '[A-Z]{2}' > $tmpfile # [9]
        iconv -f cp1252 -t utf-8 $1 | grep -vE '[A-Z]{2}' > $tmpfile # [9]
        #iconv -f cp1251 -t utf-8 $1 | grep -vE '[A-Z]{2}' > $tmpfile # [9]
    else
        iconv -f $encoding -t utf-8 $1 | grep -vE '[A-Z]{2}' > $tmpfile # [9]
    fi

    i=1
    rm -f $2
    while read line ; do
        [ -f .keep_running ] || break
        word=$(echo $line | sed 's/[/].*\r//g' | sed "s/'/ /g") # [2]
        if [[ "$word" =~ ^[a-z] && $word != *"."* ]] ; then # [4,5]
            word=$(echo $word | sed -e 's/./\L\0/g') # [7]
            # XXX CB: break hyphens (second sed)?
            for w in $(echo $word | sed 's/\r//g' | sed 's/-/ /g'); do
                echo $w >> $2
            done
        fi
    done < $tmpfile
}

echo "[$0] splitting '$1' into $NJ files for parallel processing..."
split -de -n l/$NJ --additional-suffix '.temp' $1 $PREFIX

echo -n "[$0] creating wordlist..."
i=0
for fin in $(ls ${PREFIX}*.temp) ; do
    fout=$(echo $fin | sed 's/.temp/.text/g')
    (pre_process $fin $fout)&
    printf " %03d" $i
    i=$((i+1))
done
echo

echo "[$0] waiting for subprocesses (this may take a while)..."
for pid in $(jobs -p) ; do
    wait $pid
done

echo "[$0] merging split files into '$2'..."
rm -f $2
for i in $(seq 0 $((NJ-1))) ; do
    index=$(printf "%02d" $i)
    cat ${PREFIX}${index}.text >> $2
    rm -f ${PREFIX}${index}.{temp,text}
done

tmpfile=$(mktemp)
sort $2 | uniq > $tmpfile # sort in alphabetical order and remove duplicates
mv $tmpfile $2
echo "[$0] done!"
exit 0
