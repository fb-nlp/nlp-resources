#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# author: feb 2019
# cassio batista - cassio.batista.13@gmail.com

import sys
import re
import threading
import time
import icu

N_THREADS = 6

def strip_line_into_words(tid, llist, wlist):
	counter = 0
	# https://stackoverflow.com/questions/10665591/how-to-remove-list-elements-in-a-for-loop-in-python
	while llist:
		phrase = llist.pop(0)
		if ':' in phrase or '=' in phrase:
			continue
		words = phrase.split()
		if len(words) < 2:
			continue
		for w in words:
			if '<' in w or re.search('\d', w) is not None: # or '>' in w:
				continue
			counter += 1
			wlist.append(w.strip().lower().replace('ü','u')) # FIXME no rstrip()
	wlist.sort() # FIXME make it unique using set()
	print('thread %d has finished' % tid)

if __name__=='__main__':
	if len(sys.argv) != 3:
		print('A script to extract a word list from a trained n-gram ' + \
				'language mode (LM) in ARPA format')
		print('NOTE: the script will read only the 1-gram isolated words')
		print('usage: $ python3 %s <lm_arpa> <wlist_out>' % sys.argv[0])
		print('\t<lm_arpa>:   input file')
		print('\t<wlist_out>: output file')
		sys.exit(1)

	with open(sys.argv[1], 'r') as f:
		print('loading a fucking huge lm from \'%s\'' % sys.argv[1])
		in_list = f.readlines()
		lines_list = []
		for line in in_list:
			if '2-grams:' in line:
				break
			lines_list.append(line)
		num_lines = len(lines_list)
	print('lm loaded. %d lines after bigram started.' % num_lines)
	
	words_list = []
	threads = [] # "thread pool"
	step = num_lines // N_THREADS
	tid = 0
	for i in range(0, num_lines, step):
		words_list.append([])  # a unique list (passed to a unique thread) is appended

		# avoiding out of bounds
		last_element = step;
		if (i+step) > num_lines:
			last_element = len(lines_list)

		# create and start thread t
		t = threading.Thread(target=strip_line_into_words,
			args=(tid, list(lines_list[0:last_element]), words_list[-1])) # FIXME passing by reference
		t.start()
		threads.append(t)
		tid += 1

		del lines_list[0:last_element]

	# convert list of small lists of words into a single big set of words
	print('waiting for threads to finish...')
	wordlist = set()
	while threads:
		for i in range(len(threads)):
			if not threads[i].is_alive():
				wlist = words_list.pop(i)
				for word in wlist:
					wordlist.add(word)
				del threads[i]
				break
			time.sleep(0.1)
	wordlist = list(wordlist)
	print('writing %d words extracted from lm to \'%s\'' % (len(wordlist), sys.argv[2]))

	# sort unicode strings - https://stackoverflow.com/q/1097908
	collator = icu.Collator.createInstance(icu.Locale('pt_BR.UTF-8'))
	with open(sys.argv[2], 'w') as f:
		for word in sorted(wordlist, key=collator.getSortKey):
			f.write(word.strip('/') + '\n')
### EOF ###
