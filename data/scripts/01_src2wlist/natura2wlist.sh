#!/bin/bash
#
# Grupo FalaBrasil (2019)
# Universidade Federal do Pará (UFPA). Belém, Brazil.
#
# A dumb script to convert the Natura dictionary (UMinho)
# for European Portuguese to a list of words.
#
# Author: Mar 2018
# Cassio Batista - https://cassota.gitlab.io/
# last edited: oct 2019
#
# "References"
# [1] # http://natura.di.uminho.pt/download/sources/Dictionaries/wordlists/
# [2] https://stackoverflow.com/questions/18602234/
#     sed-to-remove-everything-after-in-file-using-command
# [3] https://stackoverflow.com/questions/10358547/
#     grep-for-contents-after-pattern/10358949#10358949
# [4] https://stackoverflow.com/questions/18041761/
#     bash-need-to-test-for-alphanumeric-string
# [5] https://stackoverflow.com/questions/229551/string-contains-in-bash
# [6] https://stackoverflow.com/questions/18488270/
#     how-to-check-the-first-character-in-a-string-in-unix
# [7] https://stackoverflow.com/questions/3785375/
#     how-to-convert-text-file-to-lowercase-in-unix-but-in-utf-8
# [8] https://rimuhosting.com/knowledgebase/linux/misc/trapping-ctrl-c-in-bash
# [9] # https://www.portugal-a-programar.pt/forums/topic/
#       19646-lista-de-palavras-dicion%C3%A1rio-de-palavras-em-
#       portugu%C3%AAs-de-portugal/?do=findComment&comment=311910

NJ=6
PREFIX='NATURA'

if test $# -ne 2 ; then
    echo "$0 is a dumb script to convert the Natura (UMinho) official"
    echo "dictionary for European Portuguese to a list of words."
    echo
    echo "usage: $0 <natura_pt-br.dic> <wlist.txt>"
    echo "  <natura.dic> is the Natura UMinho dict for PT_EU" # [1]
    echo "  <wlist.txt>  is a word list extracted from the dictionary"
    exit 1
fi

touch .keep_running
trap ctrl_c INT # [8]
function ctrl_c() {
    echo ""
    echo "** Trapped CTRL-C" > /dev/null
    rm -f .keep_running
    sleep 0.2
    rm -f *.temp *.text
    exit 0
}

function pre_process() {
    encoding=$(file -i $1 | grep -oP '(?<=charset=).*$') # [3]
    tmpfile=$(mktemp)
    if [[ "$encoding" == "utf-8" ]] ; then
        cp $1 > $tmpfile
    else
        iconv -f $encoding -t utf-8 $1 | grep -vE '[A-Z]{2}' > $tmpfile # [9]
    fi
    
    i=1
    rm -f $2
    while read line ; do
        [ -f .keep_running ] || break
        word=$(echo $line | sed 's/[/].*\r//g' | sed "s/'/ /g") # [2]
        if [[ "$word" =~ ^[a-z] && $word != *"."* ]] ; then # [4,5]
            word=$(echo $word | sed -e 's/./\L\0/g') # [7]
            for w in $word ; do
                echo $w | sed 's/\r//g' |\
                    sed 's/ü/u/g' |\
                    sed 's/ám/am/g' |\
                    sed 's/ón/ôn/g' |\
                    sed 's/óm/ôm/g' >> $2
            done
    #if not 'éns' in word:
    #	word = word.replace('én','ên')
    #if not 'ém\n' in word and not 'ém-' in word:
    #	word = word.replace('ém','êm')
        fi
    done < $tmpfile 
}


# split
echo "[$0] splitting '$1' into $NJ files for parallel processing..."
split -de -n l/$NJ --additional-suffix '.temp' $1 $PREFIX

echo -n "[$0] creating wordlist..."
i=0
for fin in $(ls ${PREFIX}*.temp) ; do
    fout=$(echo $fin | sed 's/.temp/.text/g')
    (pre_process $fin $fout)&
    printf " %03d" $i
    i=$((i+1))
done
echo

echo "[$0] waiting for subprocesses (this may take a while)..."
for pid in $(jobs -p) ; do
    wait $pid
done

# merge
rm -f $2
echo "[$0] merging split files into '$2'..."
for i in $(seq 0 $((NJ-1))) ; do
    index=$(printf "%02d" $i)
    cat ${PREFIX}${index}.text >> $2
    rm -f ${PREFIX}${index}.{temp,text}
done

tmpfile=$(mktemp)
sort $2 | uniq > $tmpfile # sort in alphabetical order and remove duplicates
mv $tmpfile $2
echo "[$0] done!"
exit 0
