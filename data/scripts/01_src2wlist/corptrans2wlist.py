#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# author: feb 2019
# cassio batista - cassio.batista.13@gmail.com

import sys
import os

if __name__=='__main__':
	if len(sys.argv) != 3:
		print('A script to extract a word list from FalaBrasil\'s text ' + \
				'transcriptions from audio corpora')
		print('usage: $ python3 %s <fb_audio_corpora_dir> <wlist_out>' % sys.argv[0])
		print('\t<fb_audio_corpora_dir>: path to folder which contains all audio corpora (https://gitlab.com/fb-audio-corpora)')
		print('\t<wlist_out>:            output file')
		sys.exit(1)

	# https://stackoverflow.com/questions/3964681/find-all-files-in-a-directory-with-extension-txt-in-python
	wlist = set()
	for root, dirs, files in os.walk(sys.argv[1]):
		for f in files:
			if f.endswith('.txt'):
				sys.stdout.write('\r' + f)
				fname = os.path.join(root, f)
				with open(fname, 'r') as f:
					words = f.read().split() # NOTE: assuming a 1-line file
				for word in words:
					wlist.add(word.lower().replace('ü','u'))
				sys.stdout.flush()

	# NOTE: wlist is already unique due to set() obj
	# TODO: make it sorted first
	with open(sys.argv[2], 'w') as f:
		for word in wlist:
			f.write(word + '\n')
	print('\rdone! word list written to %s' % sys.argv[2])
