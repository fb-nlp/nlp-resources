# Recursos prontos para NLP

O arquivo `mostfreq.txt` refere-se ao baixado pelo script
`00_acquisition/get_mostfreq.txt`. Veja a pasta [`data/`](../data) para maiores
informações.

## Dicionários fonéticos

```text
# UTF-8                                  # ASCII
alinhavem   a l i~ J  a v e~ j~          alinhavem   a l ii jm  a v ee jj    
alinhavemos a l i~ J  a v e~ m u s       alinhavemos a l ii jm  a v ee m u s 
alinhaves   a l i~ J  a v i s            alinhaves   a l ii jm  a v i s      
alinhavo    a l i~ J  a v u              alinhavo    a l ii jm  a v u        
alinhavos   a l i~ J  a v u s            alinhavos   a l ii jm  a v u s      
alinhavou   a l i~ J  a v o w            alinhavou   a l ii jm  a v o w      
alinhe      a l i~ J  i                  alinhe      a l ii jm  i            
alinhei     a l i~ J  e j                alinhei     a l ii jm  e j          
alinheis    a l i~ J  e j  s             alinheis    a l ii jm  e j  s       
alinhem     a l i~ J  e~ j~              alinhem     a l ii jm  ee jj        
```

```bash
# UTF-8
$ java -jar $HOME/fb-gitlab/fb-nlp/nlp-generator/fb_nlplib.jar -g \
    -i ../data/mostfreq.txt -o res/lexicon.utf8.dict # -t 8 -p 2>/dev/null

# ASCII (para uso com o CMU Sphinx, por exemplo)
$ java -jar $HOME/fb-gitlab/fb-nlp/nlp-generator/fb_nlplib.jar -g -a \
    -i ..data/mostfreq.txt -o res/lexicon.ascii.dict # -t 8 -p 2>/dev/null
```


## Dicionário silábico

```text
alinhavem    a-li-nha-vem
alinhavemos  a-li-nha-ve-mos
alinhaves    a-li-nha-ves
alinhavo     a-li-nha-vo
alinhavos    a-li-nha-vos
alinhavou    a-li-nha-vou
alinhe       a-li-nhe
alinhei      a-li-nhei
alinheis     a-li-nheis
alinhem      a-li-nhem
```

```bash
$ java -jar $HOME/fb-gitlab/fb-nlp/nlp-generator/fb_nlplib.jar -s \
    -i ../data/mostfreq.txt -o res/syllables.txt # -t 8 -p 2>/dev/null
```

## Modelo de linguagem trigrama
A N-gram language model built with the
[SRILM](http://www.speech.sri.com/projects/srilm/download.html) toolkit. For
training, the [CETENFolha](https://www.linguateca.pt/cetenfolha/index_info.html)
corpora was used, consisting of 1.6 million sentences extracted from Folha de S.
Paulo Newspaper and compiled by the Center for Computational Linguistics of the
University of São Paulo ([NILC-USP](http://www.nilc.icmc.usp.br/nilc/index.php)). 
The dictionary used in training was UFPAdic3.0 with 64,972 words. The model is
perplexed 170 and the smoothing technique used was Witten-Bell discounting. 

[![FalaBrasil](../doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](../doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2020)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Cassio Batista - https://cassota.gitlab.io/    
