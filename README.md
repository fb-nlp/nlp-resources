# Recursos prontos para NLP

Este repositório armazena a maioria dos recursos de texto utilizados nas mais
diversaas aplicações de processamento de fala e processamento de linguagem
natural em Português Brasileiro:

- `res/lexicon.utf8.dict`: dicionário fonético com 38 fonemas em formato UTF-8
- `res/lexicon.ascii.dict`: dicionário fonético com 38 fonemas em formato ASCII
- `res/syllables.txt`: dicionário de separação silábica
- `res/lm.3gram.arpa`: modelo de linguagem trigrama no formato ARPA

Veja o README na pasta [`res/`](./res) para maiores informações.

Tenha em mente que os dicionários fonéticos e silábico foram gerados com base
em uma lista de palavras mais frequentes do Português Brasileiro,
baixada pelo script `data/scripts/00_acquisition/get_mostfreq.sh`, oriunda
do OpenSubtitles.org (graças ao projeto
[FrequencyWords](https://github.com/hermitdave/FrequencyWords)). Veja o README
na pasta [`data/`](./data) para maiores informações.

[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2020)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Cassio Batista - https://cassota.gitlab.io/    
